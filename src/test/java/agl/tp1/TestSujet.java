package agl.tp1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

public class TestSujet {
	
	public Groupe g = new Groupe("groupe");
	public Sujet s = new Sujet("suj1");
	

	@BeforeEach
	public void affecteVoeux() {
		HashMap<Integer, Sujet> v = new HashMap<Integer, Sujet>();
		v.put(1, s);
		g.setVoeux(v);
	}
	
	@Test
	public void testAffectation() {
		assertEquals(g.getVoeux().get(1).toString(), s.toString());
	}
	

}
